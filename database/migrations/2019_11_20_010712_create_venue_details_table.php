<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVenueDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venue_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('venue_id');
            $table->text('venue_lat');
            $table->text('venue_long');
            $table->string('email');
            $table->string('contactno');
            $table->text('description');
            $table->integer('book_a_table')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venue_details');
    }
}
