<?php
Route::group(['prefix' => '/'], function () {
    Route::get("", "IndexController@index")->name("index");
    Route::get("/venue", "VenueController@index")->name("venue");
});
