@extends('partials.template')

@section('body')

<!-- Slider -->
<section class="section-slide">
    <div class="wrap-slick1">
        <div class="slick1">
            <div class="item-slick1" style="background-image: url(image/slider1.png);">
                <div class="container h-full">
                    <div class="flex-col-l-m h-full p-t-100 p-b-30 respon5">

                        <div class="layer-slick1 animated visible-false" data-appear="fadeInUp" data-delay="800">
                            <h2 class="ltext-201 cl2 p-t-19 p-b-43 respon1">
                                Book your favourite restaurant
                            </h2>
                        </div>

                        <div class="layer-slick1 animated visible-false" data-appear="zoomIn" data-delay="1600">
                            <a href="product.html" class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04 resbtn">
                                View All Restaurants
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item-slick1" style="background-image: url(image/slider2.png);">
                <div class="container h-full">
                    <div class="flex-col-l-m h-full p-t-100 p-b-30 respon5">

                        <div class="layer-slick1 animated visible-false" data-appear="lightSpeedIn" data-delay="800">
                            <h2 class="ltext-201 cl2 p-t-19 p-b-43 respon1">
                                Intermezzo Ristorante
                            </h2>
                        </div>

                        <div class="layer-slick1 animated visible-false" data-appear="slideInUp" data-delay="1600">
                            <a href="product.html" class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04 resbtn">
                                View All Restaurants
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item-slick1" style="background-image: url(image/slider3.png);">
                <div class="container h-full">
                    <div class="flex-col-l-m h-full p-t-100 p-b-30 respon5">

                        <div class="layer-slick1 animated visible-false" data-appear="rotateInUpRight" data-delay="800">
                            <h2 class="ltext-201 cl2 p-t-19 p-b-43 respon1">
                                Postales Spanish Restaurant
                            </h2>
                        </div>

                        <div class="layer-slick1 animated visible-false" data-appear="rotateIn" data-delay="1600">
                            <a href="product.html" class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04 resbtn">
                                View All Restaurants
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!----------------------------------------------------------------------------------------------------------->
<section class="bg0 p-t-20 p-b-20 m-b-10">
    <h2 class="mtext-111 cl2 p-b-5 text-center  ltext-108 cl0 txt-left ">Restaurants</h2>
    <p class="stext-113 cl6 p-b-10 txt-center">
        From award-winning fine dining restaurants to informal restaurants.
    </p>
    <hr class="txt-center" style="display: block; width: 80px; height: 3px; background: #17c2ba;; margin: 20px auto;">
    <div class="container">
        <div class="row">

            <!--First Item-->
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                <div class="all-meal">
                    <div class="top">
                        <a href="{{URL::route('venue')}}"><div class="bg-gradient"></div></a>
                        <div class="top-img">
                            <img src="{{ asset('image/meals/img-1.jpg') }}" alt="">
                        </div>
                        <div class="logo-img">
                            <img src="{{ asset('image/meals/logo-2.jpg') }}" alt="">
                        </div>
                        <div class="top-text">
                            <div class="heading"><h4><a href="{{URL::route('venue')}}">Intermezzo Ristorante</a></h4></div>
                            <div class="sub-heading">
                                <h5><a href="{{URL::route('venue')}}">Italian</a></h5>
                            </div>
                        </div>
                    </div>
                    <div class="bottom">
                        <div class="bottom-text">
                            <div class="delivery"><i class="fa fa-map"></i>No.1 Martin Place, Sydney</div>
                            <div class="star">
                                <a href="{{URL::route('venue')}}d" class="btn resbtn col-xs-12 col-md-12" >Book a Table</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>



@endsection
