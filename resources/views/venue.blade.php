@extends('partials.template')
<style>
    .nav-item> a{
        color:black;
    }
    .nav-item>  a.active {
        color: #17c2ba;

        border-bottom:1px solid #17c2ba;
    }

    .food-card {
        background: #fff8f7;
        padding: 20px 10px 20px 10px;
        margin-bottom: 20px;
        -webkit-transition: all 0.3s ease 0s;
        -moz-transition: all 0.3s ease 0s;
        -o-transition: all 0.3s ease 0s;
        transition: all 0.3s ease 0s
    }

    @media (min-width: 600px) {
        .food-card {
            padding: 30px 40px 30px 40px
        }
    }

    .food-card p {
        margin: 0
    }

    .food-card .price-tag {
        font-size: 20px
    }

    .food-card-title {
        position: relative;
        margin-bottom: 5px
    }

    .food-card-title>* {
        margin: 0
    }

    .food-card-title::after {
        content: "";
        display: block;
        position: absolute;
        left: 0;
        top: 40%;
        width: 100%;
        height: 1px;
        background-color: #363636
    }

    .food-card-title h4 {
        background: #fff8f7;
        z-index: 1;
        padding-right: 10px;
        -webkit-transition: all 0.3s ease 0s;
        -moz-transition: all 0.3s ease 0s;
        -o-transition: all 0.3s ease 0s;
        transition: all 0.3s ease 0s
    }

    .food-card-title h3 {
        background: #fff8f7;
        z-index: 1;
        padding-left: 10px;
        -webkit-transition: all 0.3s ease 0s;
        -moz-transition: all 0.3s ease 0s;
        -o-transition: all 0.3s ease 0s;
        transition: all 0.3s ease 0s
    }

    .food-card-title .price-tag {
        z-index: 1
    }

    .food-card:hover {
        background: #fff;
        box-shadow: 0px 10px 20px 0px rgba(8, 6, 89, 0.1)
    }

    .food-card:hover .food-card-title h3,
    .food-card:hover .food-card-title h4 {
        background: #fff
    }

    .img-fluid {
        border: 1px solid #dcdcdc;
        border-radius: 3px;
        float: left;
        height: 59px;
        padding: 3px;
        width: 59px;
    }
    .restaurant-detailed-header {
        bottom: 0;
        left: 0;
        right: 0;
        position:absolute;
        padding: 65px 0 17px;
        background: -moz-linear-gradient(top, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.83) 100%);
        background: -webkit-linear-gradient(top, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.83) 100%);
        background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.83) 100%);
    }

    .card-back{
        padding: 10px;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }


    .business-hours .opening-hours li.today {
        color: #17c2ba;
    }

    .google-maps {
        position: relative;
        padding-bottom: 25%;
        height: 0;
        overflow: hidden;
    }
    @media only screen and (max-width: 600px){
        .google-maps {
            position: relative;
            padding-bottom: 75% !important;
        }
    }
    .google-maps iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100% !important;
        height: 100% !important;
    }

    .info-body{
        margin-top: 20px;
        padding: 10px;
    }

    .info-body p{
        padding: 10px;
        text-align: justify;
    }

    .info-body h4{
        margin-top: 10px;
        padding-left: 10px;
    }


    /** Rating CSS **/
    .btn-grey{
        background-color:#D8D8D8;
        color:#FFF;
    }
    .rating-block{
        background-color:#FAFAFA;
        border:1px solid #EFEFEF;
        padding:25px;
        border-radius:3px;
    }
    .bold{
        font-weight:700;
    }
    .padding-bottom-7{
        padding-bottom:7px;
    }
    .review-block{
        background-color:#FAFAFA;
        border:1px solid #EFEFEF;
        padding:15px;
        border-radius:3px;
        margin-bottom:15px;
    }
    .review-block-name{
        font-size:12px;
        margin:10px 0;
    }
    .review-block-date{
        font-size:12px;
    }
    .review-block-rate{
        font-size:13px;
        margin-bottom:15px;
    }
    .review-block-title{
        font-size:15px;
        font-weight:700;
        margin-bottom:10px;
    }
    .review-block-description{
        font-size:13px;
    }
    .progress-bar-success{
        background-color: #5cb85c;
    }
    .progress-bar-primary{
        background-color: #428bca;
    }
    .progress-bar-info{
        background-color: #5bc0de;
    }
    .progress-bar-warning{
        background-color: #f0ad4e
    }
    .progress-bar-danger{
        background-color: #d9534f;
    }
    .btn-rescol{
        background-color:#17c2ba;
        border-color:#17c2ba;
    }

</style>
@section('body')
    <!-- Title page -->
    <section class="bg-img1 p-lr-15 p-tb-160" style="background-image: url('image/banner1.png'); position: relative;">
        <div class="restaurant-detailed-header">
            <div class="container ">
                <div class="row">
                    <div class="col-md-8">
                        <img class="img-fluid"  src="image/blogo.jpg" style="margin-right:10px;">
                        <h2 class="mb-0 bottom-align-text" style="color:white;">Intermezzo Restorante</h2>
                        <p class="text-white mb-1"><i class="icofont-location-pin"></i> Sydney, NSW <span class="badge badge-success">OPEN</span></p>
                    </div>
                    <div class="col-md-3">
                        <div class="text-right">
                            <button class="btn btn-success" type="button"><i class="fa fa-calendar"></i> Book a Table</button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section  style="padding: 10px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active"  data-toggle="pill" href="#info" role="tab" aria-controls="pills-gallery" aria-selected="false">Restaurant Info</a>

                        </li>
                        <li class="nav-item">
                            <a class="nav-link"  data-toggle="pill" href="#menu" role="tab" aria-controls="pills-order-online" aria-selected="true">Menu</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"  data-toggle="pill" href="#gallery" role="tab" aria-controls="pills-restaurant-info" aria-selected="false">Gallery</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"  data-toggle="pill" href="#review" role="tab" aria-controls="pills-book" aria-selected="false">Review</a>
                        </li>
                    </ul>
                </div>
            </div>
    </section>
    <section class="p-a-20 p-t-30 m-b-20">
        <div class="container">
            <div class="tab-content">
                <!-- This is the tab of the menu items-->

                <div id="menu" class="tab-pane">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-back">
                                <h2 style="text-align: center; margin-top: 10px;">Starters</h2>
                                <div class="row">
                                    <div class="col-lg-6 m-t-20" >
                                        <div class="media align-items-center food-card">
                                            <img class="mr-3 mr-sm-4" src="image/food1.png" alt="">
                                            <div class="media-body">
                                                <div class="d-flex justify-content-between food-card-title">
                                                    <h4>Roasted Marrow</h4>
                                                    <h3 class="price-tag">$32</h3>
                                                </div>
                                                <p>Whales and darkness moving form cattle</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 m-t-20" >
                                        <div class="media align-items-center food-card">
                                            <img class="mr-3 mr-sm-4" src="image/food1.png" alt="">
                                            <div class="media-body">
                                                <div class="d-flex justify-content-between food-card-title">
                                                    <h4>Roasted Marrow</h4>
                                                    <h3 class="price-tag">$32</h3>
                                                </div>
                                                <p>Whales and darkness moving form cattle</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="media align-items-center food-card">
                                            <img class="mr-3 mr-sm-4" src="image/food1.png" alt="">
                                            <div class="media-body">
                                                <div class="d-flex justify-content-between food-card-title">
                                                    <h4>Roasted Marrow</h4>
                                                    <h3 class="price-tag">$32</h3>
                                                </div>
                                                <p>Whales and darkness moving form cattle</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="media align-items-center food-card">
                                            <img class="mr-3 mr-sm-4" src="image/food1.png" alt="">
                                            <div class="media-body">
                                                <div class="d-flex justify-content-between food-card-title">
                                                    <h4>Roasted Marrow</h4>
                                                    <h3 class="price-tag">$32</h3>
                                                </div>
                                                <p>Whales and darkness moving form cattle</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card m-t-30 card-back">
                                <h2 style="text-align: center; margin-top: 10px;">Soups</h2>
                                <div class="row">
                                    <div class="col-lg-6 m-t-20" >
                                        <div class="media align-items-center food-card">
                                            <img class="mr-3 mr-sm-4" src="image/food1.png" alt="">
                                            <div class="media-body">
                                                <div class="d-flex justify-content-between food-card-title">
                                                    <h4>Roasted Marrow</h4>
                                                    <h3 class="price-tag">$32</h3>
                                                </div>
                                                <p>Whales and darkness moving form cattle</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 m-t-20" >
                                        <div class="media align-items-center food-card">
                                            <img class="mr-3 mr-sm-4" src="image/food1.png" alt="">
                                            <div class="media-body">
                                                <div class="d-flex justify-content-between food-card-title">
                                                    <h4>Roasted Marrow</h4>
                                                    <h3 class="price-tag">$32</h3>
                                                </div>
                                                <p>Whales and darkness moving form cattle</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="media align-items-center food-card">
                                            <img class="mr-3 mr-sm-4" src="image/food1.png" alt="">
                                            <div class="media-body">
                                                <div class="d-flex justify-content-between food-card-title">
                                                    <h4>Roasted Marrow</h4>
                                                    <h3 class="price-tag">$32</h3>
                                                </div>
                                                <p>Whales and darkness moving form cattle</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="media align-items-center food-card">
                                            <img class="mr-3 mr-sm-4" src="image/food1.png" alt="">
                                            <div class="media-body">
                                                <div class="d-flex justify-content-between food-card-title">
                                                    <h4>Roasted Marrow</h4>
                                                    <h3 class="price-tag">$32</h3>
                                                </div>
                                                <p>Whales and darkness moving form cattle</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div id="info" class="tab-pane active">
                    <div class="row">

                        <div class="col-md-8 col-sm-12 card card-back" >
                            <div class="google-maps">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3312.8543033653186!2d151.20540831547598!3d-33.86764552643076!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12ae407edb3615%3A0x41e16a613d3461c6!2sIntermezzo%20Italian%20Restaurant!5e0!3m2!1sen!2sau!4v1569894806432!5m2!1sen!2sau" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                            </div>
                            <div class="info-body">
                                <h4>What Makes Intermezzo Italian Restaurant Special</h4>
                                <p> The first Italian restaurant ever allowed to face Martin Place and extends onto the colonnades of the Italian Renaissance GPO building and under the 8 story glass atrium, providing al fresco dining and a world class Italian-style setting. </p>
                                <p>Sydney’s best Italian cuisine with housemade pasta, desserts and gelato and an extensive and award-winning wine list comprising over 200 premium varieties of wine.</p>

                                <h4>The Best Italian Restaurant in Sydney CBD</h4>
                                <p>Intermezzo Italian Restaurant opened in April 2006 as Sydney’s ultimate Italian dining destination, offering grand al fresco dining and the best Italian cuisine in Sydney CBD.</p>
                                <p>Guests can enjoy views of Martin Place plaza and designer boutiques from the alfresco tables on the colonnade, the sophisticated ambience under the 8 story glass atrium inside the GPO building, or the intimacy of the heritage front room amongst all the action from the open kitchen, for a memorable dining experience no matter the occasion.</p>

                                <h4>Restaurant and Catering Awards for Excellence</h4>
                                <p>Best Italian Restaurant in Sydney, Formal – 2010, 2015</p>


                            </div>
                        </div>

                        <div class="col-md-3  col-sm-12  m-l-30">
                            <div class="card bg-light mb-3 card-back">
                                <div class="card-header text-white text-uppercase" style="background-color: #17c2ba;"><i class="fa fa-home"></i> Contact</div>
                                <div class="card-body">
                                    <p>3 rue des Champs Elysées</p>
                                    <p>75008 PARIS</p>
                                    <p>France</p>
                                    <p>Email : email@example.com</p>
                                    <p>Tel. +33 12 56 11 51 84</p>

                                </div>

                            </div>

                            <div class="business-hours card card-back bg-light mb-3 ">
                                <div class="card-header text-white text-uppercase" style="background-color: #17c2ba;"><i class="fa fa-home"></i> Opening Hours</div>
                                <div class="card-body">
                                    <ul class="list-unstyled opening-hours">
                                        <li>Sunday <span class="pull-right">Closed</span></li>
                                        <li>Monday <span class="pull-right">9:00-22:00</span></li>
                                        <li>Tuesday <span class="pull-right">9:00-22:00</span></li>
                                        <li>Wednesday <span class="pull-right">9:00-22:00</span></li>
                                        <li>Thursday <span class="pull-right">9:00-22:00</span></li>
                                        <li>Friday <span class="pull-right">9:00-23:30</span></li>
                                        <li>Saturday <span class="pull-right">14:00-23:30</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                <div id="gallery" class="tab-pane fade">



                </div>


                <div id="review" class="tab-pane fade">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="rating-block">
                                <h4>Average user rating</h4>
                                <h2 class="bold padding-bottom-7">4.3 <small>/ 5</small></h2>
                                <button type="button" class="btn btn-rescol btn-sm" aria-label="Left Align">
                                    <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                </button>
                                <button type="button" class="btn btn-rescol btn-sm" aria-label="Left Align">
                                    <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                </button>
                                <button type="button" class="btn btn-rescol btn-sm" aria-label="Left Align">
                                    <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                </button>
                                <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                                    <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                </button>
                                <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                                    <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <h4>Rating breakdown</h4>
                            <div class="pull-left">
                                <div class="pull-left" style="width:45px; line-height:1;">
                                    <div style="height:9px; margin:5px 0;">5 <span><i class="fa fa-star" aria-hidden="true"></i></span></div>
                                </div>
                                <div class="pull-left" style="width:260px;">
                                    <div class="progress" style="height:9px; margin:8px 0;">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width: 1000%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="pull-right" style="margin-left:25px;">1</div>
                            </div>
                            <div class="pull-left">
                                <div class="pull-left" style="width:45px; line-height:1;">
                                    <div style="height:9px; margin:5px 0;">4 <span><i class="fa fa-star" aria-hidden="true"></i></span></div>
                                </div>
                                <div class="pull-left" style="width:260px;">
                                    <div class="progress" style="height:9px; margin:8px 0;">
                                        <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: 80%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="pull-right" style="margin-left:25px;">1</div>
                            </div>
                            <div class="pull-left">
                                <div class="pull-left" style="width:45px; line-height:1;">
                                    <div style="height:9px; margin:5px 0;">3 <span><i class="fa fa-star" aria-hidden="true"></i></span></div>
                                </div>
                                <div class="pull-left" style="width:260px;">
                                    <div class="progress" style="height:9px; margin:8px 0;">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="5" style="width: 60%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="pull-right" style="margin-left:25px;">0</div>
                            </div>
                            <div class="pull-left">
                                <div class="pull-left" style="width:45px; line-height:1;">
                                    <div style="height:9px; margin:5px 0;">2 <span><i class="fa fa-star" aria-hidden="true"></i></span></div>
                                </div>
                                <div class="pull-left" style="width:260px;">
                                    <div class="progress" style="height:9px; margin:8px 0;">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="5" style="width: 40%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="pull-right" style="margin-left:25px;">0</div>
                            </div>
                            <div class="pull-left">
                                <div class="pull-left" style="width:45px; line-height:1;">
                                    <div style="height:9px; margin:5px 0;">1 <span><i class="fa fa-star" aria-hidden="true"></i></span></div>
                                </div>
                                <div class="pull-left" style="width:260px;">
                                    <div class="progress" style="height:9px; margin:8px 0;">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="5" style="width: 20%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="pull-right" style="margin-left:25px;">0</div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-10">
                            <hr/>
                            <div class="review-block">
                                <div class="row">
                                    <div class="col-sm-3 text-center">
                                        <img src="http://dummyimage.com/60x60/666/ffffff&text=No+Image" class="img-rounded">
                                        <div class="review-block-name"><a href="#">nktailor</a></div>
                                        <div class="review-block-date">January 29, 2016<br/>1 day ago</div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="review-block-rate">
                                            <button type="button" class="btn btn-rescol btn-xs" aria-label="Left Align">
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            </button>
                                            <button type="button" class="btn btn-rescol btn-xs" aria-label="Left Align">
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            </button>
                                            <button type="button" class="btn btn-rescol btn-xs" aria-label="Left Align">
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            </button>
                                            <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            </button>
                                            <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            </button>
                                        </div>
                                        <div class="review-block-title">this was nice in buy</div>
                                        <div class="review-block-description">this was nice in buy. this was nice in buy. this was nice in buy. this was nice in buy this was nice in buy this was nice in buy this was nice in buy this was nice in buy</div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-sm-3 text-center">
                                        <img src="http://dummyimage.com/60x60/666/ffffff&text=No+Image" class="img-rounded">
                                        <div class="review-block-name"><a href="#">nktailor</a></div>
                                        <div class="review-block-date">January 29, 2016<br/>1 day ago</div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="review-block-rate">
                                            <button type="button" class="btn btn-rescol btn-xs" aria-label="Left Align">
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            </button>
                                            <button type="button" class="btn btn-rescol btn-xs" aria-label="Left Align">
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            </button>
                                            <button type="button" class="btn btn-rescol btn-xs" aria-label="Left Align">
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            </button>
                                            <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            </button>
                                            <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            </button>
                                        </div>
                                        <div class="review-block-title">this was nice in buy</div>
                                        <div class="review-block-description">this was nice in buy. this was nice in buy. this was nice in buy. this was nice in buy this was nice in buy this was nice in buy this was nice in buy this was nice in buy</div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-sm-3 text-center">
                                        <img src="http://dummyimage.com/60x60/666/ffffff&text=No+Image" class="img-rounded">
                                        <div class="review-block-name"><a href="#">nktailor</a></div>
                                        <div class="review-block-date">January 29, 2016<br/>1 day ago</div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="review-block-rate">
                                            <button type="button" class="btn btn-rescol btn-xs" aria-label="Left Align">
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            </button>
                                            <button type="button" class="btn btn-rescol btn-xs" aria-label="Left Align">
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            </button>
                                            <button type="button" class="btn btn-rescol btn-xs" aria-label="Left Align">
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            </button>
                                            <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            </button>
                                            <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            </button>
                                        </div>
                                        <div class="review-block-title">this was nice in buy</div>
                                        <div class="review-block-description">this was nice in buy. this was nice in buy. this was nice in buy. this was nice in buy this was nice in buy this was nice in buy this was nice in buy this was nice in buy</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection
